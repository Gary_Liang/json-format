import 'package:moment_dart/moment_dart.dart';

const String innerModelClassName = 'UserClassModel';

class InnerModel {
  /// 名称，当为对象时，就是类型
  final String name;

  /// 类型，当类型为非对象模型时，如‘String’，那么属性只有一条为这个String
  InnerType type = InnerType.nil;
  bool isHasDecimal = false;
  bool isHasMoment = false;

  /// 属性列表
  List<InnerProperty>? propertys;

  String capitalize(String text) {
    if (text.isEmpty) {
      return text;
    }
    return text[0].toUpperCase() + text.substring(1);
  }

  InnerModel(this.name, dynamic data,
      {String suggestClassName = innerModelClassName,}) {
    if (data is Map) {
      type = InnerType.object;
      List<InnerProperty> propertys = [];
      for (var e in data.entries) {
        String key = e.key;
        dynamic value = e.value;
        InnerProperty property = InnerProperty(key, value,
            suggestClassName: capitalize("${key}Info"));
        if (property.type == InnerType.decimal) {
          isHasDecimal = true;
        }
        if (property.type == InnerType.moment) {
          isHasMoment = true;
        }

        propertys.add(property);
      }
      this.propertys = propertys;
    } else if (data is List) {
      type = InnerType.list;
      List<InnerProperty> p = [];
      for (var e in data) {
        InnerProperty property =
            InnerProperty('', e, suggestClassName: suggestClassName);

        if (property.type == InnerType.decimal) {
          isHasDecimal = true;
        }
        if (property.type == InnerType.moment) {
          isHasMoment = true;
        }
        p.add(property);
      }
      propertys = p;
    } else if (data is String) {
      propertys = [InnerProperty('', data)];
    } else if (data is num) {
      num n = data;
      if (data.toInt() != n) {
        isHasDecimal = true;
      }
      propertys = [InnerProperty('', data)];
    } else if (data is bool) {
      propertys = [InnerProperty('', data)];
    } else if (data is Moment) {
      isHasMoment = true;
      propertys = [InnerProperty('', data)];
    } else {
      InnerProperty property = InnerProperty('', data);
      if (property.type == InnerType.decimal) {
        isHasDecimal = true;
      }
      if (property.type == InnerType.moment) {
        isHasMoment = true;
      }
      propertys = [property];
    }
  }
}

enum InnerType {
  /// null
  nil,
  bool,
  int,
  string,
  list,
  object,
  decimal,
  moment,
}

class InnerProperty {
  InnerType type = InnerType.nil;
  dynamic value;
  String name;
  bool optional;
  InnerModel? subModel;

  InnerProperty(this.name, dynamic value,
      {String suggestClassName = innerModelClassName, this.optional = true}) {
    if (value is Map) {
      type = InnerType.object;
    } else if (value is List) {
      type = InnerType.list;
    } else if (value is String) {
      try {
        DateTime d=DateTime.parse(value);
        type = InnerType.moment;
      } catch (e) {
        print("moment ${e.toString()}");
        type = InnerType.string;
      }
    } else if (value is num) {
      num n = value;
      if (n.toInt() == n) {
        type = InnerType.int;
      } else {
        type = InnerType.decimal;
      }
    } else if (value is bool) {
      type = InnerType.bool;
    } else if (value is Moment) {
      type = InnerType.moment;
    } else {
      // null
      type = InnerType.nil;
    }
    this.value = _parseValue(value, suggestClassName: suggestClassName);
    if (type == InnerType.list) {
      dynamic innerItem = this.value;
      while (innerItem is List && innerItem.isNotEmpty) {
        innerItem = innerItem.first;
      }
      if (innerItem is InnerModel) {
        subModel = innerItem;
      }
    } else if (type == InnerType.object) {
      subModel = this.value;
    }
  }

  dynamic _parseValue(dynamic value,
      {String suggestClassName = innerModelClassName}) {
    if (value is Map) {
      return InnerModel(suggestClassName, value,
          suggestClassName: suggestClassName);
    } else if (value is List) {
      List arr = value;
      if (arr.isNotEmpty) {
        dynamic first = arr.first;
        if (first is Map || first is List) {
          List values = [];
          for (var item in arr) {
            dynamic itemValue =
                _parseValue(item, suggestClassName: suggestClassName);
            values.add(itemValue);
          }
          return values;
        } else {
          return value;
        }
      } else {
        return value;
      }
    } else {
      return value;
    }
  }
}
