import 'package:flutter/material.dart';
import 'package:json_format/res/assets_res.dart';

class FirstRefresh extends StatelessWidget {

  final String? image;
  const FirstRefresh({
    Key? key, this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(
      height: double.infinity,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          const Expanded(
            flex: 2,
            child: SizedBox(),
          ),
          SizedBox(
            width:  70,
            height: 70,
            child: Image(
              image:   AssetImage(image ??AssetsRes.ICON_STATUS_LOADING)  ,
            ),
          ),
          const Text("正在处理",
              style: TextStyle(fontSize: 14)),
          const Expanded(
            flex: 3,
            child: SizedBox(),
          ),
        ],
      ),);
  }
}