import 'package:animated_styled_widget/animated_styled_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:styled_widget/styled_widget.dart';

import '../index.dart';

/// 控制栏
class ControlWidget extends GetView<HomeController> {
  const ControlWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Styled.widget(
            child: <Widget>[
      btn(const Text("校验").fontSize(16), onPressed: () {
        bool isTrue = controller.isJSON;
        controller.statusTxt.value = "校验${isTrue ? "成功" : "失败"}";
      }),
      SizedBox(
        width: 10.w,
      ),
      btn(const Text("格式化").fontSize(16), onPressed: () {
        debugPrint("格式化!");
        controller.onFormatJson();
      }),
      SizedBox(
        width: 10.w,
      ),
      btn(const Text("转dart实体").fontSize(16), onPressed: () {
        debugPrint("转dart实体!");
        controller.onChangeDart();
      }),
      SizedBox(
        width: 10.w,
      ),
      btn(const Text("转kotlin实体").fontSize(16), onPressed: () {
        debugPrint("转kotlin实体!");
        controller.onChangeKotlin();
      }),
      SizedBox(
        width: 10.w,
      ),
      btn(const Text("转OC实体").fontSize(16), onPressed: () {
        debugPrint("转OC实体!");
        controller.onChangeOC();
      }),
      SizedBox(
        width: 10.w,
      ),
      btn(const Text("转Swift实体").fontSize(16), onPressed: () {
        debugPrint("转Swift实体!");
        controller.onChangeSwift();
      }),
      SizedBox(
        width: 10.w,
      ),
    ].toRow(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min))
        .padding(top: 12.w);
  }

  Widget btn(Widget child, {VoidCallback? onPressed}) {
    Style btnStyle = Style(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 8.w),
        backgroundDecoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: [
              Color(0xFFD8D9DB),
              Color(0xFFFFFFFF),
              Color(0xFFFDFDFD),
            ],
                stops: [
              0,
              0.8,
              1
            ])),
        shapeBorder: RectangleShapeBorder(
            borderRadius:
                DynamicBorderRadius.all(DynamicRadius.circular(50.toPXLength)),
            border: const DynamicBorderSide(
              color: Color(0xFF8F9092),
              width: 1,
              //begin: (50*pi).toPXLength
            )),
        shadows: [
          const ShapeShadow(
            offset: Offset(0, -6),
            blurRadius: 4,
            color: Color(0xFFFEFEFE),
          ),
          const ShapeShadow(
            offset: Offset(0, -4),
            blurRadius: 4,
            color: Color(0xFFCECFD1),
          ),
          const ShapeShadow(
            offset: Offset(0, 6),
            blurRadius: 8,
            color: Color(0xFFd6d7d9),
          ),
          const ShapeShadow(
            offset: Offset(0, 4),
            blurRadius: 3,
            spreadRadius: 1,
            color: Color(0xFFFCFCFC),
          ),
        ],
        insetShadows: [
          const ShapeShadow(
            offset: Offset(1, 1),
            blurRadius: 3,
            color: Color(0xFFCECFD1),
          )
        ],
        textStyle: DynamicTextStyle(
          letterSpacing: 1.toPXLength,
          fontSize: Dimension.min(300.toPercentLength, 18.toPXLength),
          fontWeight: FontWeight.w500,
          color: const Color(0xFF606060),
        ),
        textAlign: TextAlign.center,
        mouseCursor: SystemMouseCursors.click);
    return StyledButton(
      style: btnStyle,
      pressedStyle: btnStyle.copyWith(
        backgroundDecoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: [
              Color(0xFFD8D9DB),
              //Color(0xFFFFFFFF),
              Color(0xFFFDFDFD),
            ],
                stops: [
              0,
              //0.8,
              1
            ])),
        insetShadows: [
          const ShapeShadow(
            offset: Offset(0, 0),
            blurRadius: 3,
            spreadRadius: 1,
            color: Color(0xFF888888),
          ),
          const ShapeShadow(
            offset: Offset(0, 0),
            blurRadius: 10,
            spreadRadius: 3,
            color: Color(0xFFCECFD1),
          ),
        ],
      ),
      hoveredStyle: btnStyle.copyWith(
        insetShadows: [
          const ShapeShadow(
            offset: Offset(0, 0),
            blurRadius: 3,
            spreadRadius: 3,
            color: Color(0xFFCECFD1),
          )
        ],
      ),
      onPressed: onPressed,
      child: child,
    );
  }
}
