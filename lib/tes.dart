import 'package:decimal/decimal.dart';
import 'package:moment_dart/moment_dart.dart';

class UserClassModel {
  int code;
  DataInfo data;

  UserClassModel({ required this.code,required this.data,});

  factory UserClassModel.fromMap(Map<String, dynamic> json) {
    return UserClassModel(
      code : json['code'] ?? 0,
      data:DataInfo.fromMap(json['data']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'code': code,
      'data': data?.toJson(),
    };
  }
}


class DataInfo {
  List<ListInfo> list;
  PageQueryInfo pageQuery;
  Decimal sumRemain;

  DataInfo({ required this.list,required this.pageQuery,required this.sumRemain,});

  factory DataInfo.fromMap(Map<String, dynamic> json) {
    return DataInfo(
      list : ((json['list'] ?? []) as List<dynamic>).map((e) => ListInfo.fromMap(e)).toList(),
      pageQuery:PageQueryInfo.fromMap(json['pageQuery']),
      sumRemain : json['sumRemain'] == null?Decimal.zero:Decimal.tryParse(json['sumRemain'])??Decimal.zero,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'list': list?.map((e) => e.toJson()).toList(),
      'pageQuery': pageQuery?.toJson(),
      'sumRemain': sumRemain,
    };
  }
}


class ListInfo {
  String id;
  int total;
  List<PromoListInfo> promoList;
  MerchantInfo merchant;
  IssueInfo issue;
  List<IssueFinanceRecordInfo> issueFinanceRecord;
  List<dynamic> issueFinanceProduct;
  int cashReceived;
  int cashPendingConfirm;
  int productReceived;
  int received;
  int remain;

  ListInfo({ required this.id,required this.total,required this.promoList,required this.merchant,required this.issue,required this.issueFinanceRecord,required this.issueFinanceProduct,required this.cashReceived,required this.cashPendingConfirm,required this.productReceived,required this.received,required this.remain,});

  factory ListInfo.fromMap(Map<String, dynamic> json) {
    return ListInfo(
      id : json['_id'] ?? '',
      total : json['total'] ?? 0,
      promoList : ((json['promoList'] ?? []) as List<dynamic>).map((e) => PromoListInfo.fromMap(e)).toList(),
      merchant:MerchantInfo.fromMap(json['merchant']),
      issue:IssueInfo.fromMap(json['issue']),
      issueFinanceRecord : ((json['issueFinanceRecord'] ?? []) as List<dynamic>).map((e) => IssueFinanceRecordInfo.fromMap(e)).toList(),
      issueFinanceProduct : (json['issueFinanceProduct'] ?? []) as List<dynamic>,
      cashReceived : json['cashReceived'] ?? 0,
      cashPendingConfirm : json['cashPendingConfirm'] ?? 0,
      productReceived : json['productReceived'] ?? 0,
      received : json['received'] ?? 0,
      remain : json['remain'] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'total': total,
      'promoList': promoList?.map((e) => e.toJson()).toList(),
      'merchant': merchant?.toJson(),
      'issue': issue?.toJson(),
      'issueFinanceRecord': issueFinanceRecord?.map((e) => e.toJson()).toList(),
      'issueFinanceProduct': issueFinanceProduct,
      'cashReceived': cashReceived,
      'cashPendingConfirm': cashPendingConfirm,
      'productReceived': productReceived,
      'received': received,
      'remain': remain,
    };
  }
}


class PromoListInfo {
  String id;
  List<int> departmentList;
  String trueName;

  PromoListInfo({ required this.id,required this.departmentList,required this.trueName,});

  factory PromoListInfo.fromMap(Map<String, dynamic> json) {
    return PromoListInfo(
      id : json['_id'] ?? '',
      departmentList : ((json['departmentList'] ?? []) as List<int>).map((v) => v).toList(),
      trueName : json['trueName'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'departmentList': departmentList,
      'trueName': trueName,
    };
  }
}


class MerchantInfo {
  String id;
  String name;

  MerchantInfo({ required this.id,required this.name,});

  factory MerchantInfo.fromMap(Map<String, dynamic> json) {
    return MerchantInfo(
      id : json['_id'] ?? '',
      name : json['name'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'name': name,
    };
  }
}


class IssueInfo {
  String id;
  Moment? createTime;

  IssueInfo({ required this.id,required this.createTime,});

  factory IssueInfo.fromMap(Map<String, dynamic> json) {
    return IssueInfo(
      id : json['_id'] ?? '',
      createTime : json['createTime'] == null?null:Moment.tryParse(json['createTime']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'createTime': createTime,
    };
  }
}


class IssueFinanceRecordInfo {
  String id;
  String type;
  int amount;
  List<String> imageList;
  bool isConfirm;
  Moment? confirmTime;
  String desc;
  String issueFinance;
  String user;
  Moment? createTime;
  int v;
  String confirmUser;
  Moment? receiveTime;

  IssueFinanceRecordInfo({ required this.id,required this.type,required this.amount,required this.imageList,required this.isConfirm,required this.confirmTime,required this.desc,required this.issueFinance,required this.user,required this.createTime,required this.v,required this.confirmUser,required this.receiveTime,});

  factory IssueFinanceRecordInfo.fromMap(Map<String, dynamic> json) {
    return IssueFinanceRecordInfo(
      id : json['_id'] ?? '',
      type : json['type'] ?? '',
      amount : json['amount'] ?? 0,
      imageList : ((json['imageList'] ?? []) as List<String>).map((v) => v).toList(),
      isConfirm : json['isConfirm'] ?? false,
      confirmTime : json['confirmTime'] == null?null:Moment.tryParse(json['confirmTime']),
      desc : json['desc'] ?? '',
      issueFinance : json['issueFinance'] ?? '',
      user : json['user'] ?? '',
      createTime : json['createTime'] == null?null:Moment.tryParse(json['createTime']),
      v : json['__v'] ?? 0,
      confirmUser : json['confirmUser'] ?? '',
      receiveTime : json['receiveTime'] == null?null:Moment.tryParse(json['receiveTime']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'type': type,
      'amount': amount,
      'imageList': imageList,
      'isConfirm': isConfirm,
      'confirmTime': confirmTime,
      'desc': desc,
      'issueFinance': issueFinance,
      'user': user,
      'createTime': createTime,
      '__v': v,
      'confirmUser': confirmUser,
      'receiveTime': receiveTime,
    };
  }
}


class PageQueryInfo {
  int total;
  int page;
  int size;

  PageQueryInfo({ required this.total,required this.page,required this.size,});

  factory PageQueryInfo.fromMap(Map<String, dynamic> json) {
    return PageQueryInfo(
      total : json['total'] ?? 0,
      page : json['page'] ?? 0,
      size : json['size'] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'total': total,
      'page': page,
      'size': size,
    };
  }
}