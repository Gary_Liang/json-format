import 'package:json_format/common/models/model.dart';
import 'package:moment_dart/moment_dart.dart';

extension InnerPropertyExtension on InnerProperty {
  String get valueTypeList {
    dynamic firstData = value.first;
    String firstName = name.replaceAll("_", "");
    if (firstData is String) {
      return "$firstName : ((json['$name'] ?? []) as List<dynamic>).map((v) => v as String).toList(),";
    } else if (firstData is num) {
      num n = firstData;
      if (firstData.toInt() != n) {
        return "$firstName : ((json['$name'] ?? []) as List<dynamic>).map((v) => v as Decimal).toList(),";
      } else {
        return "$firstName : ((json['$name'] ?? []) as List<dynamic>).map((v) => v as int).toList(),";
      }
    } else if (firstData is bool) {
      return "$firstName : ((json['$name'] ?? []) as List<dynamic>).map((v) => v as bool).toList(),";
    } else if (firstData is Moment) {
      return "$firstName : ((json['$name'] ?? []) as List<dynamic>).map((v) => v as Moment).toList(),";
    } else {
      return "$firstName : ((json['$name'] ?? []) as List<dynamic>).map((v) => v).toList(),";
    }
  }

  String valueList(String type) {
    String firstName = name.replaceAll("_", "");
    return "$firstName : ((json['$name'] ?? []) as List<$type>).map((v) => v).toList(),";
  }

  String get dynamicData {
    String firstName = name.replaceAll("_", "");
    return "$firstName : (json['$name'] ?? []) as List<dynamic>,";
  }
}
