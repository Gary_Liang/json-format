
import 'dart:convert';

extension JSONHelper on String? {
  bool get isJSON {
    if (this == null) return false;
    try {
      jsonDecode(this!);
    } catch (error) {
      return false;
    }
    return true;
  }

  String? get first {
    if (this == null) return null;
    if (this!.isEmpty) return "";
    return this![0];
  }
}