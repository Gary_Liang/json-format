import 'package:json_format/common/models/model.dart';

class ModelKotlinConvert {
  /// \n
  static String wrap = '\n';

  /// \t
  static String space = "    ";

  static String parseModelV2(InnerModel model) {
    String output = '';
    output += wrap;
    // 嵌套的模型
    List<InnerModel> nestModels = [];
    if (model.propertys?.isNotEmpty == true) {
      output += 'data class ${model.name} (';
      for (var property in model.propertys!) {
        if (property.subModel != null) {
          nestModels.add(property.subModel!);
        }
        output += wrap;
        String writePro = parsePropertyV2(property);
        output += writePro;
      }
      output += wrap;
      output += ')';
    } else {}
    if (nestModels.isNotEmpty) {
      output += wrap;
      output += '{';
      for (var model in nestModels) {
        output += wrap;
        output += parseModelV2(model);
        output += wrap;
      }
      output += wrap;
      output += '}';
    }
    return output;
  }

  static String parsePropertyV2(InnerProperty property) {
    String output = '';
    String name = property.name.replaceAll("_", "");
    switch (property.type) {
      case InnerType.bool:
        {
          output += space;
          output += 'val $name: Boolean,';
        }
        break;
      case InnerType.int:
        {
          output += space;
          output += 'val $name: Int,';
        }
        break;
      case InnerType.moment:
        {
          output += space;
          output += 'val $name: Date,';
        }
        break;
      case InnerType.decimal:
        {
          output += space;
          output += 'val $name: Double,';
        }
        break;
      case InnerType.string:
        {
          output += space;
          output += 'val $name: String,';
        }
        break;
      case InnerType.list:
        {
          output += space;
          List arr = property.value;
          String nameT = '';
          if (arr.isNotEmpty) {
            dynamic first = arr.first;
            String t;
            if (property.subModel != null) {
              t = property.subModel!.name;
            } else {
              t = arr.first.runtimeType.toString();
            }
            dynamic inner = first;
            while (inner is List) {
              t = 'List<${t == "int" ? "Int" : t}>';
              if (inner.isNotEmpty) {
                inner = inner.first;
              }
            }
            nameT = '<${t == "int" ? "Int" : t}>';
            output += 'val ${name}:List$nameT,';
          } else {
            output += 'val ${name}:List<Any>,';
          }
        }
        break;
      case InnerType.object:
        {
          output += space;
          InnerModel _model = property.value;
          if (_model.propertys?.isNotEmpty == true) {
            output += 'val ${name}:${_model.name},';
          } else {
            output += 'val ${name}:Any,';
          }
        }
        break;
      case InnerType.nil:
        {
          output += space;
          output += 'val $name: Any,';
        }
        break;
      default:
        break;
    }
    return output;
  }
}
