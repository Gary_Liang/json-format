import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_format/common/models/json.util.dart';
import 'package:json_format/common/models/string_extension_json.dart';
import 'package:path_provider/path_provider.dart';

class HomeController extends GetxController {
  HomeController();

    TextEditingController editController= TextEditingController(text: "");

  var text = "".obs;

  _initData() {
    update(["home"]);
  }

  void onTap() {}

  // @override
  // void onInit() {
  //   super.onInit();
  // }

  @override
  void onReady() {
    super.onReady();
    _initData();
  }

  // @override
  // void onClose() {
  //   super.onClose();
  // }

  var isJson = false.obs;
  var outputText = "".obs;
  var statusTxt = "状态：--".obs;
  var isLoad = false.obs;
  var hasToJson = true.obs;

  void onCheckJson() {}

  /// 输入的是否为json格式
  bool get isJSON {
    bool hasJson = text?.value.isNotEmpty ?? false;
    if (!hasJson) return false;
    return text.value.isJSON;
  }

  Future<bool> onFormatJson() async {
    if (text?.value.isEmpty ?? false) return false;

    isLoad.value = true;
    statusTxt.value = "正在处理";
    Future.delayed(const Duration(seconds: 2), () async {
      String? formatJson = await JsonUtil.format(text?.value);
      if (kDebugMode) {
        print("_formatJson $formatJson");
      }
      outputText.value = formatJson ?? "";
      statusTxt.value = "格式化成功";
      isLoad.value = false;
    });
    return true;
  }

  Future<bool> onChangeDart() async {
    if (text?.value.isEmpty ?? false) return false;
    isLoad.value = true;
    Future.delayed(const Duration(seconds: 2), () async {
      outputText.value = await JsonUtil.onChangeDartModel(text?.value ?? "",
          hasToJson: hasToJson.value);
      statusTxt.value = "转换 dart实体成功";
      isLoad.value = false;
    });
    return true;
  }

  Future<bool> onChangeKotlin() async {
    if (text?.value.isEmpty ?? false) return false;
    isLoad.value = true;
    Future.delayed(const Duration(seconds: 2), () async {
      outputText.value = await JsonUtil.onChangeKotlinModel(text?.value ?? "");
      statusTxt.value = "转换 kotlin实体成功";
      isLoad.value = false;
    });
    return true;
  }

  Future<bool> onChangeOC() async {
    if (text?.value.isEmpty ?? false) return false;
    isLoad.value = true;
    Future.delayed(const Duration(seconds: 2), () async {
      outputText.value = await JsonUtil.onChangeOCModel(text?.value ?? "");
      statusTxt.value = "转换 oc实体成功";
      isLoad.value = false;
    });
    return true;
  }

  Future<bool> onChangeSwift() async {
    if (text?.value.isEmpty ?? false) return false;
    isLoad.value = true;
    Future.delayed(const Duration(seconds: 2), () async {
      outputText.value = await JsonUtil.onChangeSwiftModel(text?.value ?? "");
      statusTxt.value = "转换 swift实体成功";
      isLoad.value = false;
    });
    return true;
  }

  Future<void> export() async {
    if (outputText?.value.isEmpty ?? false) return;
    statusTxt.value = "正在处理导出";
    Future.delayed(const Duration(seconds: 2), () async {
      Directory? downloadDir = GetPlatform.isMacOS
          ? (await getApplicationDocumentsDirectory())
          : (await getDownloadsDirectory());
      if (downloadDir != null) {
        String createDirPath = downloadDir.path;
        File file = File('$createDirPath/my_model.dart');
        bool exists = await file.exists();
        if (!exists) {
          file = await file.create();
        }
        file.writeAsString(outputText.value ?? "");
        statusTxt.value = "导出成功=>${'$createDirPath/my_model.dart'}";
      }
    });
  }

  void showLoadingDialog() {
    Get.dialog(
      const Center(
        child: CircularProgressIndicator(),
      ),
      barrierDismissible: false, // 设置为false，防止用户点击外部关闭对话框
    );
  }

  void hideLoadingDialog() {
    Get.back(); // 关闭对话框
  }
}
