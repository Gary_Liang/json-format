
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:json_format/pages/home/index.dart';

class MyRootApp extends StatelessWidget {
  const MyRootApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return ScreenUtilInit(
        designSize: const Size(800, 800),
    // builder: (context,child){
      //       return  GetMaterialApp(
      //         title: 'Flutter Demo',
      //         theme: ThemeData(
      //           primarySwatch: Colors.blue,
      //         ),
      //         home: const HomePage(),
      //       );
      // },
      child: GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomePage(),
    ),);
  }
}