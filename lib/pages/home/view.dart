import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:styled_widget/styled_widget.dart';
import 'package:json_format/pages/home/widgets/bottom_control.dart';
import 'package:json_format/pages/home/widgets/content.dart';
import 'package:json_format/pages/home/widgets/control.dart';

import 'index.dart';

///主页
class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return const _HomeViewGetX();
  }
}

class _HomeViewGetX extends GetView<HomeController> {
  const _HomeViewGetX({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      id: "home",
      builder: (controller) {
        return Scaffold(
          body: Styled.widget(
                  child: <Widget>[
            const ControlWidget(),
            const ContentWidget().expanded(),
            BottomControlWidget()
          ].toColumn())
              .backgroundColor(const Color(0xFFE0E0E0))
              .width(ScreenUtil().screenWidth),
        );
      },
    );
  }
}
