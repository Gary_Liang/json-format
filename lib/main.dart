import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:flutter/material.dart';
import 'package:json_format/pages/my_app.dart';

void main() {
  runApp(const MyRootApp());

  doWhenWindowReady(() {
    // const initialSize = Size(1280, 800);
    const initialSize = Size(800, 800);
    appWindow.minSize = initialSize;
    appWindow.size = initialSize;
    appWindow.alignment = Alignment.center;
    appWindow.title = "Json Format";
    appWindow.show();
  });
}
