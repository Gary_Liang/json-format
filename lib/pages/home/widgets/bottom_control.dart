import 'package:animated_styled_widget/animated_styled_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:json_format/common/models/json.util.dart';
import 'package:styled_widget/styled_widget.dart';

import '../index.dart';

/// 控制栏
class BottomControlWidget extends GetView<HomeController> {
  BottomControlWidget({super.key});

  final TextEditingController _textController =
      TextEditingController(text: JsonUtil.className);

  @override
  Widget build(BuildContext context) {
    // return Container(
    //   child: Row(
    //     children: [
    //
    //       SizedBox(
    //         width: 20.w,
    //       ),
    //       btn(const Text("复制").fontSize(14).textColor(Colors.pink), onPressed: () {
    //         Clipboard.setData(
    //             ClipboardData(text: controller.outputText.value ?? ""));
    //         controller.statusTxt.value = "复制成功";
    //       }),
    //       SizedBox(
    //         width: 10.w,
    //       ),
    //       btn(const Text("导出").fontSize(14), onPressed: () {
    //         controller.export();
    //       }),
    //       SizedBox(
    //         width: 10.w,
    //       ),
    //       btn(
    //           Obx(() => Text("Dart添加ToJson:${controller.hasToJson.value}")
    //               .fontSize(14)
    //               .textColor(Colors.pink)), onPressed: () {
    //         controller.hasToJson.value = !controller.hasToJson.value;
    //       }),
    //       SizedBox(
    //         width: 10.w,
    //       ),
    //      Expanded(child: Container( height: 50,
    //        margin: EdgeInsets.only(top: 20),
    //        child: TextField(
    //          controller: _textController,
    //          style: const TextStyle(
    //              fontSize: 16,
    //              color: Colors.cyan,
    //              overflow: TextOverflow.ellipsis,
    //              fontWeight: FontWeight.w600),
    //          buildCounter: (context,
    //              {required currentLength, maxLength, required isFocused}) {
    //            // 根据是否焦点和当前长度显示不同样式
    //            return const Text('');
    //          },
    //          decoration: const InputDecoration(
    //            hintText: '实体名',
    //            border: InputBorder.none,
    //          ),
    //          maxLength: 20,
    //          onChanged: (v) {
    //            JsonUtil.className = v;
    //          },
    //        ),
    //      ) ),
    //       Styled.widget(
    //           child: Obx(() => Text(controller.statusTxt.value ?? '状态:')
    //               .fontSize(14)
    //               .textColor(Colors.pink)
    //               .fontWeight(FontWeight.w500)
    //               .textAlignment(TextAlign.right)
    //               .alignment(Alignment.centerRight)
    //               .padding(right: 20.w, top: 10.w))).expanded()
    //     ],
    //   ),
    // );
    return Styled.widget(
            child: <Widget>[
      SizedBox(
        width: 20.w,
      ),
      btn(const Text("复制").fontSize(14).textColor(Colors.pink), onPressed: () {
        Clipboard.setData(
            ClipboardData(text: controller.outputText.value ?? ""));
        controller.statusTxt.value = "复制成功";
      }),
      SizedBox(
        width: 10.w,
      ),
      btn(const Text("导出").fontSize(14), onPressed: () {
        controller.export();
      }),
      SizedBox(
        width: 10.w,
      ),
      btn(
          Obx(() => Text("Dart添加ToJson:${controller.hasToJson.value}")
              .fontSize(14)
              .textColor(Colors.pink)), onPressed: () {
        controller.hasToJson.value = !controller.hasToJson.value;
      }),
      SizedBox(
        width: 10.w,
      ),
              Expanded(child: Container( height: 50,
                child: TextField(
                  controller: _textController,
                  style: const TextStyle(
                      fontSize: 16,
                      color: Colors.cyan,
                      overflow: TextOverflow.ellipsis,
                      fontWeight: FontWeight.w600),
                  buildCounter: (context,
                      {required currentLength, maxLength, required isFocused}) {
                    // 根据是否焦点和当前长度显示不同样式
                    return const Text('');
                  },
                  decoration: const InputDecoration(
                    hintText: '实体名',
                    border: InputBorder.none,
                  ),
                  maxLength: 20,
                  onChanged: (v) {
                    JsonUtil.className = v;
                  },
                ),
              ) ),
      Styled.widget(
          child: Obx(() => Text(controller.statusTxt.value ?? '状态:')
              .fontSize(14)
              .textColor(Colors.pink)
              .fontWeight(FontWeight.w500)
              .textAlignment(TextAlign.right)
              .alignment(Alignment.centerRight)
              .padding(right: 20.w, top: 10.w))).expanded()
    ].toRow(crossAxisAlignment: CrossAxisAlignment.start))
        .width(ScreenUtil().screenWidth)
        .padding(bottom: 12.w);
  }

  Widget btn(Widget child, {VoidCallback? onPressed}) {
    Style btnStyle = Style(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 8.w),
        backgroundDecoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: [
              Color(0xFFD8D9DB),
              Color(0xFFFFFFFF),
              Color(0xFFFDFDFD),
            ],
                stops: [
              0,
              0.8,
              1
            ])),
        shapeBorder: RectangleShapeBorder(
            borderRadius:
                DynamicBorderRadius.all(DynamicRadius.circular(50.toPXLength)),
            border: const DynamicBorderSide(
              color: Color(0xFF8F9092),
              width: 1,
              //begin: (50*pi).toPXLength
            )),
        shadows: [
          const ShapeShadow(
            offset: Offset(0, -6),
            blurRadius: 4,
            color: Color(0xFFFEFEFE),
          ),
          const ShapeShadow(
            offset: Offset(0, -4),
            blurRadius: 4,
            color: Color(0xFFCECFD1),
          ),
          const ShapeShadow(
            offset: Offset(0, 6),
            blurRadius: 8,
            color: Color(0xFFd6d7d9),
          ),
          const ShapeShadow(
            offset: Offset(0, 4),
            blurRadius: 3,
            spreadRadius: 1,
            color: Color(0xFFFCFCFC),
          ),
        ],
        insetShadows: [
          const ShapeShadow(
            offset: Offset(1, 1),
            blurRadius: 3,
            color: Color(0xFFCECFD1),
          )
        ],
        textStyle: DynamicTextStyle(
          letterSpacing: 1.toPXLength,
          fontSize: Dimension.min(300.toPercentLength, 18.toPXLength),
          fontWeight: FontWeight.w600,
          color: const Color(0xFF606060),
        ),
        textAlign: TextAlign.center,
        mouseCursor: SystemMouseCursors.click);
    return StyledButton(
      style: btnStyle,
      pressedStyle: btnStyle.copyWith(
        backgroundDecoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: [
              Color(0xFFD8D9DB),
              //Color(0xFFFFFFFF),
              Color(0xFFFDFDFD),
            ],
                stops: [
              0,
              //0.8,
              1
            ])),
        insetShadows: [
          const ShapeShadow(
            offset: Offset(0, 0),
            blurRadius: 3,
            spreadRadius: 1,
            color: Color(0xFF888888),
          ),
          const ShapeShadow(
            offset: Offset(0, 0),
            blurRadius: 10,
            spreadRadius: 3,
            color: Color(0xFFCECFD1),
          ),
        ],
      ),
      hoveredStyle: btnStyle.copyWith(
        insetShadows: [
          const ShapeShadow(
            offset: Offset(0, 0),
            blurRadius: 3,
            spreadRadius: 3,
            color: Color(0xFFCECFD1),
          )
        ],
      ),
      onPressed: onPressed,
      child: child,
    );
  }
}
