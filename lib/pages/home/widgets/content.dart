import 'package:animated_styled_widget/animated_styled_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:json_format/common/widgets/first_refresh.dart';
import 'package:markdown_widget/config/markdown_generator.dart';
import 'package:markdown_widget/widget/markdown.dart';

import '../index.dart';

/// 内容
class ContentWidget extends GetView<HomeController> {
  const ContentWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.w),
      child: Row(
        children: [
          Expanded(child: buildEditText()),
          // VerticalDivider(
          //   width: 3,
          // ),
          SizedBox(width: 20.w,),
          Expanded(child: descriptionWidget()),
        ],
      ),
    );
  }

  Widget buildEditText() {
    return textBg(
      child: TextFormField(
        expands: true,
        maxLines: null,
        textInputAction: TextInputAction.newline,
        controller: controller.editController,
        onChanged: (text) {
          controller.text.value = text;
        },

        style: const TextStyle(textBaseline: TextBaseline.alphabetic),
        decoration: const InputDecoration(
            contentPadding: EdgeInsets.all(10),
            border: InputBorder.none,
            hintText: 'Input Here...',
            hintStyle: TextStyle(color: Colors.grey)),
      ),
    );
  }

  Widget descriptionWidget() {

    return  textBg(
          child: Container(
            alignment: Alignment.centerLeft,
            child: Obx(() =>controller.isLoad.value?const FirstRefresh():
            MarkdownWidget(
                data: controller.outputText?.value.isNotEmpty==true?"```${controller.outputText?.value}" : '请在左侧添加要处理的文本',
                // config: MarkdownConfig.darkConfig,
                markdownGenerator: MarkdownGenerator(
                  richTextBuilder: (span) => Text.rich(span, textScaleFactor: 1),
                ))
            )
          ),
        ) ;
  }


  Widget  textBg({Widget? child, }) {
    Style btnStyle = Style(
        alignment: Alignment.centerLeft,
        childAlignment: Alignment.centerLeft,
        padding:   EdgeInsets.symmetric(horizontal: 20.w,vertical: 8.w),
        backgroundDecoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
                colors: [
                  Color(0xFFD8D9DB),
                  Color(0xFFFFFFFF),
                  Color(0xFFFDFDFD),
                ],
                stops: [
                  0,
                  0.8,
                  1
                ])),
        shapeBorder: RectangleShapeBorder(
            borderRadius:
            DynamicBorderRadius.all(DynamicRadius.circular(20.toPXLength)),
            border: const DynamicBorderSide(
              color: Color(0xFF8F9092),
              width: 1,
              //begin: (50*pi).toPXLength
            )),
        shadows: [
          const ShapeShadow(
            offset: Offset(0, -6),
            blurRadius: 4,
            color: Color(0xFFFEFEFE),
          ),
          const ShapeShadow(
            offset: Offset(0, -4),
            blurRadius: 4,
            color: Color(0xFFCECFD1),
          ),
          const ShapeShadow(
            offset: Offset(0, 6),
            blurRadius: 8,
            color: Color(0xFFd6d7d9),
          ),
          const ShapeShadow(
            offset: Offset(0, 4),
            blurRadius: 3,
            spreadRadius: 1,
            color: Color(0xFFFCFCFC),
          ),
        ],
        insetShadows: [
          const ShapeShadow(
            offset: Offset(1, 1),
            blurRadius: 3,
            color: Color(0xFFCECFD1),
          )
        ],
        textStyle: DynamicTextStyle(
          letterSpacing: 1.toPXLength,
          fontSize: Dimension.min(300.toPercentLength, 18.toPXLength),
          fontWeight: FontWeight.w500,
          color: const Color(0xFF606060),
        ),
        textAlign: TextAlign.left,
        mouseCursor: SystemMouseCursors.click);
    return StyledButton(
      style: btnStyle,
      hoveredStyle: btnStyle.copyWith(
        insetShadows: [
          const ShapeShadow(
            offset: Offset(0, 0),
            blurRadius: 3,
            spreadRadius: 3,
            color: Color(0xFFCECFD1),
          )
        ],
      ),
      child: child,
    );
  }
}
