import 'dart:convert';

import 'package:json_format/common/models/covert.dart';
import 'package:json_format/common/models/model.dart';
import 'package:json_format/common/models/string_extension_json.dart';
import 'package:json_format/common/models/swift/covert.swift.dart';

import 'kotlin/covert.kotlin.dart';
import 'oc/covert.oc.dart';

class JsonUtil {
  static String className="UserClassModel";
  static Future<String?> format(String? json) async {
    if (json?.isJSON == false) return null;
    var data = jsonDecode(json!);
    var output = _formatData(data);
    return output;
  }

  static String _formatData(dynamic data,
      {String box = '', String space = '', String lineHeadSpace = "    "}) {
    if (data is Map) {
      box += "{";
      String endSpace = space;
      space += lineHeadSpace;
      var keys = data.keys.toList();
      for (int i = 0; i < data.length; i++) {
        box += "\n";
        var key = keys[i];
        if (i == data.length - 1) {
          box += "$space\"$key\": ${_formatData(data[key], space: space)}";
        } else {
          box +=
              "$space\"$key\": ${_formatData(data[key], space: space)},";
        }
      }
      box += "\n";
      box += "$endSpace}";
    } else if (data is List) {
      box += "[";
      String endSpace = space;
      space += lineHeadSpace;
      for (int i = 0; i < data.length; i++) {
        box += "\n";
        var e = data[i];
        if (i == data.length - 1) {
          box += space + _formatData(e, space: space);
        } else {
          box += "$space${_formatData(e, space: space)},";
        }
      }
      box += "\n";
      box += "$endSpace]";
    } else if (data is String) {
      box += "\"$data\"";
    } else if (data is num) {
      box += "$data";
    } else if (data is bool) {
      box += "$data";
    } else {
      box += "null";
    }
    return box;
  }

  ///转换实体
  static Future<String> onChangeDartModel(String json,
      {bool hasToJson = true}) async {
    var data = jsonDecode(json);
    if (data is Map) {
      InnerModel model = InnerModel(JsonUtil.className, data);
      String output = '';
      String output2 = '';
      if (model.type == InnerType.object) {
        ModelDartConvert.isHasDecimal = false;
        ModelDartConvert.isHasMoment = false;
        output2 = ModelDartConvert.parseModelV2(model, hasToJson: hasToJson);
        if (ModelDartConvert.isHasDecimal) {
          output += "import 'package:decimal/decimal.dart';";
          output += "\n";
        }
        if (ModelDartConvert.isHasMoment) {
          output += "import 'package:moment_dart/moment_dart.dart';";
          output += "\n";
        }
      }
      return output + output2;
    } else {
      return data.toString();
    }
  }

  ///转换实体
  static Future<String> onChangeKotlinModel(String json) async {
    var data = jsonDecode(json);
    if (data is Map) {
      InnerModel model = InnerModel(JsonUtil.className, data);
      String output2 = '';
      if (model.type == InnerType.object) {
        output2 = ModelKotlinConvert.parseModelV2(model);
      }
      return output2;
    } else {
      return data.toString();
    }
  }

  ///转换实体
  static Future<String> onChangeOCModel(String json) async {
    var data = jsonDecode(json);
    if (data is Map) {
      InnerModel model = InnerModel(JsonUtil.className, data);
      String output2 = '';
      output2 += '#import <Foundation/Foundation.h>';
      ModelOCConvert.mClassTxt="";
      ModelOCConvert.mTopClassTxt="";
      ModelOCConvert.objectLis.clear();
      if (model.type == InnerType.object) {
        final data=ModelOCConvert.parseModelV2(model,isFirst: true);
        output2 +=ModelOCConvert.wrap;
        output2 += ModelOCConvert.mTopClassTxt;
        output2 +=ModelOCConvert.wrap;
        output2 += data;
        output2 +=ModelOCConvert.wrap;
        output2 +=ModelOCConvert.wrap;
        output2 +=ModelOCConvert.wrap;
        output2 += '${ModelOCConvert.wrap} .m 文件';
        output2 +=ModelOCConvert.wrap;
        output2 +=ModelOCConvert.wrap;
        output2 += ModelOCConvert.parseModelH(model);
      }
      return output2;
    } else {
      return data.toString();
    }
  }

  ///转换实体
  static Future<String> onChangeSwiftModel(String json) async {
    var data = jsonDecode(json);
    if (data is Map) {
      InnerModel model = InnerModel(JsonUtil.className, data);
      String output2 = '';
      if (model.type == InnerType.object) {
        output2 = ModelSwiftConvert.parseModelV2(model);
      }
      return output2;
    } else {
      return data.toString();
    }
  }
}

enum Language { dart, objectiveC, swift, java, kotlin }
