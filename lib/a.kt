
data class UserClassModel (
    val code: Int,
    val  data:DataInfo,
)
{

    data class DataInfo (
        val  list:List<ListInfo>,
        val  pageQuery:PageQueryInfo,
        val sumRemain: Double,
    )
    {

        data class ListInfo (
            val id: String,
            val total: Int,
            val  promoList:List<PromoListInfo>,
            val  merchant:MerchantInfo,
            val  issue:IssueInfo,
            val  issueFinanceRecord:List<IssueFinanceRecordInfo>,
            val  issueFinanceProduct:List<Any>,
            val cashReceived: Int,
            val cashPendingConfirm: Int,
            val productReceived: Int,
            val received: Int,
            val remain: Int,
        )
        {

            data class PromoListInfo (
                val id: String,
                val  departmentList:List<Int>,
                val trueName: String,
            )


            data class MerchantInfo (
                val id: String,
                val name: String,
            )


            data class IssueInfo (
                val id: String,
                val createTime: Date,
            )


            data class IssueFinanceRecordInfo (
                val id: String,
                val type: String,
                val amount: Int,
                val  imageList:List<String>,
                val isConfirm: Boolean,
                val confirmTime: Date,
                val desc: String,
                val issueFinance: String,
                val user: String,
                val createTime: Date,
                val v: Int,
                val confirmUser: String,
                val receiveTime: Date,
            )

        }


        data class PageQueryInfo (
            val total: Int,
            val page: Int,
            val size: Int,
        )

    }

}